const parse = require("pg-connection-string").parse;
const config = parse(process.env.DATABASE_URL);

console.info(
  "connect database type postgres (production)",
  `${config.database} ${config.host}:${config.port} with user: ${config.user}`
);

module.exports = ({ env }) => ({
  connection: {
    client: "postgres",
    connection: {
      host: config.host,
      port: config.port,
      database: config.database,
      user: config.user,
      password: config.password,
      ssl: {
        rejectUnauthorized: env.bool("DATABASE_SSL_SELF", false), // For self-signed certificates
      },
    },
    debug: false,
  },
});
